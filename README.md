[![pipeline status](https://gitlab.com/hsc-oh/samtools-build/badges/master/pipeline.svg)](https://gitlab.com/hsc-oh/samtools-build/commits/master)


# Package Advisories

This package is dynamically linked against:
- libbzip2
- xz
- libcurl
- zlib
- openssl
- ncurses

BCFtools is linked against:
- libgsl

## Bugs
